# heroku test

* pulled from github - automated deploy
  * https://mytest123enc123.herokuapp.com/
* pushed with heroku cli from gitlab repository
  * https://my1stherokutestongitlab.herokuapp.com/
  
 
# docker ohne heroku

    docker build -t testapp-ohne-heroku
    docker run -p 3412:3412 -d testapp-ohne-heroku
    
# build log

```
[markus@arch heroku_test]$ heroku git:remote -a my1stherokutestongitlab
set git remote heroku to https://git.heroku.com/my1stherokutestongitlab.git
[markus@arch heroku_test]$ git branch
* master
[markus@arch heroku_test]$ git push heroku master
Zähle Objekte: 19, Fertig.
Delta compression using up to 4 threads.
Komprimiere Objekte: 100% (19/19), Fertig.
Schreibe Objekte: 100% (19/19), 4.08 KiB | 2.04 MiB/s, Fertig.
Total 19 (delta 4), reused 0 (delta 0)
remote: Compressing source files... done.
remote: Building source:
remote: 
remote: -----> Node.js app detected
remote: 
remote: -----> Creating runtime environment
remote:        
remote:        NPM_CONFIG_LOGLEVEL=error
remote:        NPM_CONFIG_PRODUCTION=true
remote:        NODE_VERBOSE=false
remote:        NODE_ENV=production
remote:        NODE_MODULES_CACHE=true
remote: 
remote: -----> Installing binaries
remote:        engines.node (package.json):  unspecified
remote:        engines.npm (package.json):   unspecified (use default)
remote:        
remote:        Resolving node version 6.x...
remote:        Downloading and installing node 6.11.4...
remote:        Using default npm version: 3.10.10
remote: 
remote: -----> Restoring cache
remote:        Skipping cache restore (not-found)
remote: 
remote: -----> Building dependencies
remote:        Installing node modules (package.json)
remote:        heroku_api_test@0.0.1 /tmp/build_58eaa870e0a72318a94542e513c2c3c4
remote:        ├─┬ body-parser@1.18.2
remote:        │ ├── bytes@3.0.0
remote:        │ ├── content-type@1.0.4
remote:        │ ├─┬ debug@2.6.9
remote:        │ │ └── ms@2.0.0
remote:        │ ├── depd@1.1.1
remote:        │ ├─┬ http-errors@1.6.2
remote:        │ │ ├── inherits@2.0.3
remote:        │ │ └── setprototypeof@1.0.3
remote:        │ ├── iconv-lite@0.4.19
remote:        │ ├─┬ on-finished@2.3.0
remote:        │ │ └── ee-first@1.1.1
remote:        │ ├── qs@6.5.1
remote:        │ ├─┬ raw-body@2.3.2
remote:        │ │ └── unpipe@1.0.0
remote:        │ └─┬ type-is@1.6.15
remote:        │   └── media-typer@0.3.0
remote:        ├─┬ express@4.16.1
remote:        │ ├─┬ accepts@1.3.4
remote:        │ │ └── negotiator@0.6.1
remote:        │ ├── array-flatten@1.1.1
remote:        │ ├── content-disposition@0.5.2
remote:        │ ├── cookie@0.3.1
remote:        │ ├── cookie-signature@1.0.6
remote:        │ ├── encodeurl@1.0.1
remote:        │ ├── escape-html@1.0.3
remote:        │ ├── etag@1.8.1
remote:        │ ├── finalhandler@1.1.0
remote:        │ ├── fresh@0.5.2
remote:        │ ├── merge-descriptors@1.0.1
remote:        │ ├── methods@1.1.2
remote:        │ ├── parseurl@1.3.2
remote:        │ ├── path-to-regexp@0.1.7
remote:        │ ├─┬ proxy-addr@2.0.2
remote:        │ │ ├── forwarded@0.1.2
remote:        │ │ └── ipaddr.js@1.5.2
remote:        │ ├── range-parser@1.2.0
remote:        │ ├── safe-buffer@5.1.1
remote:        │ ├─┬ send@0.16.1
remote:        │ │ ├── destroy@1.0.4
remote:        │ │ └── mime@1.4.1
remote:        │ ├── serve-static@1.13.1
remote:        │ ├── setprototypeof@1.1.0
remote:        │ ├── statuses@1.3.1
remote:        │ ├── utils-merge@1.0.1
remote:        │ └── vary@1.1.2
remote:        ├─┬ mocha@4.0.0
remote:        │ ├── browser-stdout@1.3.0
remote:        │ ├── commander@2.11.0
remote:        │ ├── debug@3.1.0
remote:        │ ├── diff@3.3.1
remote:        │ ├── escape-string-regexp@1.0.5
remote:        │ ├─┬ glob@7.1.2
remote:        │ │ ├── fs.realpath@1.0.0
remote:        │ │ ├─┬ inflight@1.0.6
remote:        │ │ │ └── wrappy@1.0.2
remote:        │ │ ├─┬ minimatch@3.0.4
remote:        │ │ │ └─┬ brace-expansion@1.1.8
remote:        │ │ │   ├── balanced-match@1.0.0
remote:        │ │ │   └── concat-map@0.0.1
remote:        │ │ ├── once@1.4.0
remote:        │ │ └── path-is-absolute@1.0.1
remote:        │ ├─┬ growl@1.10.2
remote:        │ │ ├── UNMET PEER DEPENDENCY eslint@>=3.1.0
remote:        │ │ └─┬ eslint-plugin-node@5.2.0
remote:        │ │   ├── ignore@3.3.5
remote:        │ │   ├─┬ resolve@1.4.0
remote:        │ │   │ └── path-parse@1.0.5
remote:        │ │   └── semver@5.3.0
remote:        │ ├── he@1.1.1
remote:        │ ├─┬ mkdirp@0.5.1
remote:        │ │ └── minimist@0.0.8
remote:        │ └─┬ supports-color@4.4.0
remote:        │   └── has-flag@2.0.0
remote:        └─┬ request@2.83.0
remote:        ├── aws-sign2@0.7.0
remote:        ├── aws4@1.6.0
remote:        ├── caseless@0.12.0
remote:        ├─┬ combined-stream@1.0.5
remote:        │ └── delayed-stream@1.0.0
remote:        ├── extend@3.0.1
remote:        ├── forever-agent@0.6.1
remote:        ├─┬ form-data@2.3.1
remote:        │ └── asynckit@0.4.0
remote:        ├─┬ har-validator@5.0.3
remote:        │ ├─┬ ajv@5.2.3
remote:        │ │ ├── co@4.6.0
remote:        │ │ ├── fast-deep-equal@1.0.0
remote:        │ │ ├── json-schema-traverse@0.3.1
remote:        │ │ └─┬ json-stable-stringify@1.0.1
remote:        │ │   └── jsonify@0.0.0
remote:        │ └── har-schema@2.0.0
remote:        ├─┬ hawk@6.0.2
remote:        │ ├── boom@4.3.1
remote:        │ ├─┬ cryptiles@3.1.2
remote:        │ │ └── boom@5.2.0
remote:        │ ├── hoek@4.2.0
remote:        │ └── sntp@2.0.2
remote:        ├─┬ http-signature@1.2.0
remote:        │ ├── assert-plus@1.0.0
remote:        │ ├─┬ jsprim@1.4.1
remote:        │ │ ├── extsprintf@1.3.0
remote:        │ │ ├── json-schema@0.2.3
remote:        │ │ └─┬ verror@1.10.0
remote:        │ │   └── core-util-is@1.0.2
remote:        │ └─┬ sshpk@1.13.1
remote:        │   ├── asn1@0.2.3
remote:        │   ├── bcrypt-pbkdf@1.0.1
remote:        │   ├── dashdash@1.14.1
remote:        │   ├── ecc-jsbn@0.1.1
remote:        │   ├── getpass@0.1.7
remote:        │   ├── jsbn@0.1.1
remote:        │   └── tweetnacl@0.14.5
remote:        ├── is-typedarray@1.0.0
remote:        ├── isstream@0.1.2
remote:        ├── json-stringify-safe@5.0.1
remote:        ├─┬ mime-types@2.1.17
remote:        │ └── mime-db@1.30.0
remote:        ├── oauth-sign@0.8.2
remote:        ├── performance-now@2.1.0
remote:        ├── stringstream@0.0.5
remote:        ├─┬ tough-cookie@2.3.3
remote:        │ └── punycode@1.4.1
remote:        ├── tunnel-agent@0.6.0
remote:        └── uuid@3.1.0
remote:        
remote: 
remote: -----> Caching build
remote:        Clearing previous node cache
remote:        Saving 2 cacheDirectories (default):
remote:        - node_modules
remote:        - bower_components (nothing to cache)
remote: 
remote: -----> Build succeeded!
remote:  !     Unmet dependencies don't fail npm install but may cause runtime issues
remote:        https://github.com/npm/npm/issues/7494
remote: 
remote: -----> Discovering process types
remote:        Procfile declares types     -> (none)
remote:        Default types for buildpack -> web
remote: 
remote: -----> Compressing...
remote:        Done: 15.5M
remote: -----> Launching...
remote:        Released v3
remote:        https://my1stherokutestongitlab.herokuapp.com/ deployed to Heroku
remote: 
remote: Verifying deploy... done.
To https://git.heroku.com/my1stherokutestongitlab.git
 * [new branch]      master -> master
[markus@arch heroku_test]$ 
```


# Gitlab Runner

...
.
