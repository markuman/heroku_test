var app = require('./src/app');
var port = process.env.PORT || 3412

app.listen(port, function(){
	console.log("app listening on port 3412");
});
