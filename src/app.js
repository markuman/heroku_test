const express = require('express');
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const app = express();


const quotes = [
  { author : 'alf', text : "Das beste Mittel gegen eine gegessene Schokolade ist eine neue Schokolade."},
  { author : 'alf', text : "Ich weiß dein Angebot zu schätzen, aber lieber verschlucke ich einen Hammer"},
  { author : 'alf', text : "Die Staatsbibliothek auf Melmac hatte zwei Bücher, als sie abbrannte. Und eines war noch nicht mal ausgemalt."}
];

function return_one_quote (){
  let idx = Math.floor(Math.random() * quotes.length);
  return quotes[idx]
}

app.get('/status', jsonParser, function(req, res) {
  res.json({msg: "Hello World"})
})

app.get('/', jsonParser, function(req, res) {
  res.json(quotes);
});

app.get('/quote/random', jsonParser, function(req, res) {
  res.json(return_one_quote());
});

app.get('/quote/:id', jsonParser, function(req, res) {
  if(quotes.length <= req.params.id || req.params.id < 0) {
    res.statusCode = 404;
    return res.send('Error 404: No quote found');
  }

  var q = quotes[req.params.id];
  res.json(q);
});

module.exports = app;

