const request = require("supertest");

describe("test API calls", function() {
	
	let server;

	// Arrange
	beforeAll(function (){
		const app = require("../src/app");		
		server = app.listen(0);
	});

	afterAll(function (done) {
		server.close(function() { done(); });
	});
	
	it("responds with JSON message 'Hello World'", function(done) {
		// Act
		request(server)
			.get('/status')

			// Assert
			.expect('Content-Type', /json/)
			.expect(200)
			.then(response => {
				expect(response.body.msg).toEqual('Hello World');
				done();
			})
			
	});

	it("should response with an error, because index out of range", function(done){
		request(server)
			.get('/quote/123')

			.expect(404)
			.end(function(err, res) {
				if (err) return done.fail('This should definitly throw an error');
				done();
			});

	})

	it("should return one quote", function(done){
		request(server)
			.get('/quote/random')
			
			.expect('Content-Type', /json/)
			.expect(200)
			.then(response => {
				expect(response.body.author).toEqual("alf")
				expect(response.body.text).toEqual(jasmine.any(String))
				done();
			})
	})
	
	
});
